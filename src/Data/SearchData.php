<?php


namespace App\Data;

use App\Entity\Campus;
use DateTime;

class SearchData
{
    /**
     * @var null|Campus
     */
    public $campus;

    /**
     * @var string
     */
    public $texte = '';

    /**
     * @var null|DateTime
     */
    public $dateDebutRecherche;

    /**
     * @var null|DateTime
     */
    public $dateFinRecherche;

    /**
     * @var boolean
     */
    public $sortiesOrganisees;

    /**
     * @var boolean
     */
    public $sortiesInscrit;

    /**
     * @var boolean
     */
    public $sortiesNonInscrit;

    /**
     * @var boolean
     */
    public $sortiesPassees;
}