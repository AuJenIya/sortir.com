<?php

namespace App\Controller;

use App\Entity\Participant;
use App\Form\EditProfilType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ParticipantController extends AbstractController
{

    /**
     * @Route("/participant", name="participant")
     */
    public function index()
    {
        return $this->render('participant/list.html.twig', [
            'controller_name' => 'ParticipantController',
        ]);
    }

    /**
     * @Route("/participant/{id}", name="participant_show")
     * @param Participant $participant
     * @return Response
     */
    public function show(Participant $participant)
    {
        return $this->render('participant/show.html.twig', [
            'participant' => $participant,
            'participantConnecte' => $this->getUser()
        ]);
    }

    /**
     * @Route("/participant/edit/{id}", name="participant_edit")
     * @param Request $request
     * @param Participant $participant
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     */
    public function edit(Request $request, Participant $participant, UserPasswordEncoderInterface $passwordEncoder)
    {

        $form = $this->createForm(EditProfilType::class, $participant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $participant->setMotDePasse($passwordEncoder->encodePassword($participant, $participant->getPassword()));

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('participant_show', ['id' => $participant->getId()] );
        }

        return $this->render('participant/edit.html.twig', [
            'participant' => $participant,
            'form' => $form->createView()
        ]);
    }
}
