<?php

namespace App\Controller;

use App\Data\SearchData;
use App\Entity\Sortie;
use App\Form\SearchFormType;
use App\Form\SortieType;
use App\Repository\EtatRepository;
use App\Repository\SortieRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SortiesController extends AbstractController
{
    /**
     * @Route("/sorties", name="sorties")
     * @param SortieRepository $sortieRepository
     * @param Request $request
     * @return Response
     */
    public function listerLesSorties(SortieRepository $sortieRepository, Request $request)
    {
        $data = new SearchData();
        $form = $this->createForm(SearchFormType::class, $data);
        $form->handleRequest($request);
        $user = $this->getUser();
        $sorties = $sortieRepository->findSearch($data, $user);

        return $this->render('sorties/list.html.twig', [
            'sorties' => $sorties,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/sorties/new", name="sorties_new")
     * @param Request $request
     * @param EtatRepository $etatRepository
     * @return Response
     */
    public function creerUneSortie(Request $request, EtatRepository $etatRepository)
    {

        $sortie = new Sortie();
        $form = $this->createForm(SortieType::class, $sortie);


        $form->handleRequest($request);
        /*

        if ($form->isSubmitted() && $form->isValid()) {

            $sortie->setSiteOrganisateur($this->getUser()->getCampus());
            $sortie->setOrganisateur($this->getUser());
            $sortie->setEtat($etatRepository->find(1));

            $this->getDoctrine()->getManager()->persist($sortie);
            $this->getDoctrine()->getManager()->flush();

        }*/

        return $this->render('sorties/new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/sorties/{id}", name="sortie_show")
     * @param Sortie $sortie
     * @return Response
     */
    public function consulter(Sortie $sortie)
    {

        $inscription = false;
        $desistement = false;

        if ($sortie->getOrganisateur() != $this->getUser() && !$sortie->getParticipants()->contains($this->getUser()) && $sortie->getEtat()->getId() == 2 && $sortie->getDateCloture() > new \DateTime()) {
            $inscription = true;
        }

        if ($sortie->getEtat()->getId() == 1 || $sortie->getEtat()->getId() == 2) {
            $desistement = true;
        }

        dump($this->getUser());
        dump($sortie);

        return $this->render('sorties/show.html.twig', [
            'sortie' => $sortie,
            'utilisateur' => $this->getUser(),
            'inscription' => $inscription,
            'desistement' => $desistement
        ]);
    }

    /**
     * @Route("/sorties/inscription/{id}", name="sortie_inscription")
     * @param Sortie $sortie
     * @return Response
     */
    public function inscription(Sortie $sortie)
    {

        $message = '';

        if ($sortie->getDateCloture() > new \DateTime()) {
            $sortie->addParticipant($this->getUser());
            $sortie->setNbInscriptionsMax($sortie->getNbInscriptionsMax() - 1);

            $this->getDoctrine()->getManager()->persist($sortie);
            $this->getDoctrine()->getManager()->flush();
        } else {
            $message = 'La date limite des inscriptions est dépassée';
        }

        return $this->redirectToRoute('sortie_show', [
            'id' => $sortie->getId(),
            'message' => $message
        ]);
    }

    /**
     * @Route("/sorties/desistement/{id}", name="sortie_desistement")
     * @param Sortie $sortie
     * @return Response
     */
    public function desistement(Sortie $sortie)
    {

        if (new \DateTime() < $sortie->getDateDebut()) {
            $sortie->removeParticipant($this->getUser());

            if ($sortie->getDateCloture() > new \DateTime()) {
                $sortie->setNbInscriptionsMax($sortie->getNbInscriptionsMax() + 1);
            }

            $this->getDoctrine()->getManager()->persist($sortie);
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->redirectToRoute('sortie_show', [
            'id' => $sortie->getId()
        ]);
    }

}
