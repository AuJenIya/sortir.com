<?php

namespace App\DataFixtures;

use App\Entity\Campus;
use App\Entity\Etat;
use App\Entity\Lieu;
use App\Entity\Participant;
use App\Entity\Sortie;
use App\Entity\Ville;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker;

class AppFixtures extends Fixture
{
    private $encoder;
    private $entityManager;

    public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $entityManager) {
        $this->encoder = $encoder;
        $this->entityManager = $entityManager;
    }

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        //
        // $manager->persist($product);
        $faker = Faker\Factory::create('fr_FR');

        // Réalisation des campus

        $campus = new Campus();
        $campus->setNom('rennes');
        $manager->persist($campus);
        $campus = new Campus();
        $campus->setNom('nantes');
        $manager->persist($campus);
        $campus = new Campus();
        $campus->setNom('quimper');
        $manager->persist($campus);
        $manager->flush();

        // Création d'un utilisateur administrateur

        $admin = new Participant();
        $admin->setPseudo('aurelien22');
        $admin->setPrenom('Aurélien');
        $admin->setNom('Dincuff');
        $admin->setTelephone('0690897654');
        $admin->setMail('aurelien.dincuff@gmail.com');
        $admin->setMotDePasse($this->encoder->encodePassword($admin, 'password'));
        $admin->setAdministrateur(1);
        $admin->setActif(1);
        $admin->setCampus($this->entityManager->getRepository(Campus::class)->find(1));
        $manager->persist($admin);
        $manager->flush();

        // Création d'une liste de participants

        for ($i = 0; $i < 49; $i++) {
            $participant = new Participant();
            $participant->setPrenom($faker->firstName);
            $participant->setPseudo($participant->getPrenom() . "" . rand(10, 50));
            $participant->setNom($faker->lastName);
            $participant->setTelephone('0689763432');
            $participant->setMail($faker->email);
            $participant->setMotDePasse($this->encoder->encodePassword($participant, 'password'));
            $participant->setAdministrateur(0);
            $participant->setActif(1);
            $participant->setCampus($this->entityManager->getRepository(Campus::class)->find(rand(1,3)));
            $manager->persist($participant);
        }

        $manager->flush();

        // Création des villes

        for ($i = 0; $i < 10; $i++) {
            $ville = new Ville();
            $ville->setNom($faker->city);
            $ville->setCodePostal($faker->postcode);
            $manager->persist($ville);
        }

        $manager->flush();

        // Création des lieux

        for ($i = 0; $i < 20; $i++) {
            $lieu = new Lieu();
            $lieu->setNom($faker->streetName);
            $lieu->setRue($faker->streetAddress);
            $lieu->setVille($this->entityManager->getRepository(Ville::class)->find(rand(1, 10)));
            $manager->persist($lieu);
        }

        $manager->flush();

        // Création des états de sorties

        $etat = new Etat();
        $etat->setLibelle('Créée');
        $manager->persist($etat);
        $etat = new Etat();
        $etat->setLibelle('Ouverte');
        $manager->persist($etat);
        $etat = new Etat();
        $etat->setLibelle('Clôturée');
        $manager->persist($etat);
        $etat = new Etat();
        $etat->setLibelle('En cours');
        $manager->persist($etat);
        $etat = new Etat();
        $etat->setLibelle('Passée');
        $manager->persist($etat);
        $etat = new Etat();
        $etat->setLibelle('Annulée');
        $manager->persist($etat);

        $manager->flush();

        // Création des sorties créees

        for ($i = 0; $i < 5; $i++) {

            $sortie = new Sortie();
            $sortie->setNom($faker->text(20));
            $sortie->setNbInscriptionsMax(rand(1, 20));
            $sortie->setDateDebut($faker->dateTimeInInterval('now', '+60 days'));
            $sortie->setDateCloture($sortie->getDateDebut()->modify('-1 day'));
            $sortie->setDuree(rand(2, 5));
            $sortie->setEtat($this->entityManager->getRepository(Etat::class)->find(1));
            $sortie->setOrganisateur($this->entityManager->getRepository(Participant::class)->find(rand(2, 50)));
            $sortie->setSiteOrganisateur($this->entityManager->getRepository(Campus::class)->find(rand(1, 3)));
            $sortie->setLieu($this->entityManager->getRepository(Lieu::class)->find(rand(1, 20)));

            $manager->persist($sortie);

        }

        $manager->flush();

        // Création des sorties ouvertes

        for ($i = 0; $i < 5; $i++) {

            $sortie = new Sortie();
            $sortie->setNom($faker->text(20));
            $sortie->setNbInscriptionsMax(rand(1, 20));
            $sortie->setDateDebut($faker->dateTimeInInterval('now', '+60 days'));
            $sortie->setDateCloture($sortie->getDateDebut()->modify('-1 day'));
            $sortie->setDuree(rand(2, 5));
            $sortie->setEtat($this->entityManager->getRepository(Etat::class)->find(2));
            $sortie->setOrganisateur($this->entityManager->getRepository(Participant::class)->find(rand(2, 50)));
            $sortie->setSiteOrganisateur($this->entityManager->getRepository(Campus::class)->find(rand(1, 3)));
            $sortie->setLieu($this->entityManager->getRepository(Lieu::class)->find(rand(1, 20)));

            for ($i = 0; $i < 5; $i++) {
                $sortie->addParticipant($this->entityManager->getRepository(Participant::class)->find());
            }

            $manager->persist($sortie);

        }

        $manager->flush();
    }
}
