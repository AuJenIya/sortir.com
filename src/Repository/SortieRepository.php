<?php

namespace App\Repository;

use App\Data\SearchData;
use App\Entity\Participant;
use App\Entity\Sortie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Sortie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sortie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sortie[]    findAll()
 * @method Sortie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SortieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sortie::class);
    }

    public function findSearch(SearchData $search, Participant $participant) {

        $query = $this->createQueryBuilder('s');

        if ($search->sortiesInscrit === true && $search->sortiesNonInscrit === true) {

        } else {
            if ($search->sortiesInscrit === true) {
                $query = $query
                    ->join('s.participants', 'p')
                    ->andWhere('p.id = :z')
                    ->setParameter('z', $participant->getId());
            }

            if ($search->sortiesNonInscrit === true) {
                $query = $query
                    ->leftJoin('s.participants', 'pa')
                    ->andWhere('pa.id IS NULL')
                    ->orWhere('pa.id != :y')
                    ->setParameter('y', $participant->getId());
            }
        }

        if ($search->sortiesPassees === true) {
            $query = $query
                ->andWhere('s.dateCloture < :s ')
                ->setParameter('s', new \DateTime());
        }

        if (!empty($search->campus)) {
            $query = $query
                ->join('s.siteOrganisateur', 'so')
                ->andWhere('so.id = :p' )
                ->setParameter('p', $search->campus->getId());
        }

        if (!empty($search->texte)) {
            $query = $query
                ->andWhere('s.nom LIKE :q')
                ->setParameter('q', "%{$search->texte}%");
        }

        if ($search->sortiesOrganisees === true) {
            $query = $query
                ->join('s.organisateur', 'o')
                ->andWhere('o = :r')
                ->setParameter('r', $participant->getId());
        }

        return $query->getQuery()->getResult();

    }

    // /**
    //  * @return Sortie[] Returns an array of Sortie objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sortie
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

                if ($search->sortiesNonInscrit === true) {
                $query = $query
                    ->leftJoin('s.participants', 'pa')
                    ->andWhere('pa.id IS NULL')
                    ->orWhere('pa.id != :y')
                    ->setParameter('y', $participant->getId());
            }


    */
}
