<?php


namespace App\Form;


use App\Data\SearchData;
use App\Entity\Campus;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('campus', EntityType::class, [
                'class' => Campus::class,
                'required' => false,
                'label' => 'Campus',
                'choice_label' => 'nom',
                'choice_value' => 'id',
            ])
            ->add('texte', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Rechercher'
                ]
            ])
            /*->add('dateDebutRecherche', DateType::class, [
                'label' => 'Entre',
                'required' => false,
            ])
            ->add('dateFinRecherche', DateType::class, [
                'label' => 'et',
                'required' => false,
            ])*/
            ->add('sortiesOrganisees', CheckboxType::class, [
                'label' => 'Sorties organisées',
                'required' => false,
            ])
            ->add('sortiesInscrit', CheckboxType::class, [
                'label' => 'Sorties où je suis inscrit',
                'required' => false,
            ])
            ->add('sortiesNonInscrit', CheckboxType::class, [
                'label' => 'Sorties où je ne suis pas inscrit',
                'required' => false,
            ])
            ->add('sortiesPassees', CheckboxType::class, [
                'label' => 'Sorties passées',
                'required' => false,
            ]);

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SearchData::class,
            'method' => 'POST',
            'csrf_protection' => false
        ]);
    }

    public function getBlockPrefix()
    {
        return '';
    }
}